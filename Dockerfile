FROM openjdk:17
ARG JAR_FILE=target/*.jar
WORKDIR /app
COPY ${JAR_FILE} /app/
ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8082
